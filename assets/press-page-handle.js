function getURLParameter(name) {
  return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [null, ''])[1].replace(/\+/g, '%20')) || null;
}
function TogglePressExpanded(el) {
  var $exp = $('.recent-press-expanded');
  if(!el.hasClass("active")) {
  	$('.article.active').removeClass("active");

    var id = el.data("id");
    var pdf = el.data("pdf") + "?" + Date.now();
    var img = "https:"+el.find("img").attr("src");
    var title = el.find("h3").text();
    var desc = el.find("p").text();

    $exp.slideUp(function() {
	    $exp.find("embed").attr("src", pdf + "#zoomFitH");
	    $exp.find(".pdf-new-tab a").attr("href", pdf);
	    $exp.find(".facebook").attr("href", "https://www.facebook.com/sharer.php?caption="+encodeURI(title)+"&description="+encodeURI(desc)+"&u=https://www.cleanhappens.com/pages/press?id="+id+"&picture="+img);
	    $exp.find(".twitter").attr("href", "https://twitter.com/share?url=https://www.cleanhappens.com/pages/press?id="+id+"&text="+encodeURI("Better Life featured on "+title));
	    $exp.find(".pinterest").attr("href", "https://pinterest.com/pin/create/bookmarklet/?media="+img+"&url=https://www.cleanhappens.com/pages/press?id="+id+"&is_video=false&description="+encodeURI(title));
	    $exp.find(".tumblr").attr("href", "https://www.tumblr.com/widgets/share/tool?canonicalUrl=https://www.cleanhappens.com/pages/press?id="+id+"&title="+encodeURI(title)+"&caption="+encodeURI(desc));

	    el.addClass("active");
	    $exp.slideDown();
	});
  } else {
    el.removeClass("active");
    $exp.slideUp();
  }
}
function SetMinPressHeight() {
	$('.press-multi-slide .item').css("min-height", "");
	var h = 0;
	$('.press-multi-slide .item').each(function() {
		if($(this).height() > h) {
			h = $(this).height();
		}
	});
	$('.press-multi-slide .item').css("min-height", h+"px");
}
$(document).ready(function($) {
	$('.press-multi-slide .item').each(function() {
		var next = $(this).next();
		if(!next.length) {
			next = $(this).siblings(":first");
		}
		next.children(":first-child").clone().appendTo($(this));
		if(next.next().length > 0) {
			next.next().children(":first-child").clone().appendTo($(this));
		} else {
			$(this).siblings(":first").children(":first-child").clone().appendTo($(this));
		}
		$(this).append("<div style='clear:both;'></div>");
	});
	SetMinPressHeight();
	$('a[href="#RecentPressCarousel"]').on("click", function(e) {
		$('.article.active').removeClass("active");
		$('.recent-press-expanded').slideUp();
	});
	if(getURLParameter("id")) {
		var id = getURLParameter("id");
		$('#RecentPressCarousel .item').each(function() {
			var item = $(this);
			var art = $(this).find("article:first-child");
			if(art.data("id") == id) {
				if(item.index() > 0) {
					$('#RecentPressCarousel .item.active').removeClass("active");
				}
				$('#RecentPressCarousel .item:eq('+item.index()+')').addClass("active");
				TogglePressExpanded(art);
				return false;
			}
		});
		$('html,body').animate({ 
			scrollTop: $('#RecentPressCarousel').offset().top
		}, 1000);
	}
});
$(window).on("resize", function() {
	SetMinPressHeight();
});