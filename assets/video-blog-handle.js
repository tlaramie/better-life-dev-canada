function bxSliderMode() {

  var w = window.innerWidth;

  if(w <= 767) {

    return 'horizontal';

  } else {

    return 'vertical';

  }

}

var slider = [];

function bxSliderInit(init) {

  if(init == 'init') {

    //Gain reference to each bxslider
    $('div.alternate-carousel').each(function() {

      if(bxSliderMode() == 'vertical') {

        var slide = $(this).bxSlider({

          mode: 'vertical',
          minSlides: 3,
          maxSlides: 3,
          moveSlides: 1,
          slideMargin: 5,
          infiniteLoop: false,
          hideControlOnEnd: true

        });

      } else {

        var slide = $(this).bxSlider({

          mode: 'horizontal',
          slideWidth: 250,
          minSlides: 2,
          maxSlides: 3,
          moveSlides: 1,
          slideMargin: 0,
          infiniteLoop: false,
          hideControlOnEnd: true

        });

      }

      slider.push(slide);

    });

  } else {

    if(bxSliderMode() == 'vertical') {

      for(var x = 0; x < slider.length; x++) {

        slider[x].reloadSlider({ mode: 'vertical', minSlides: 3, maxSlides: 3, moveSlides: 1, slideMargin: 5, infiniteLoop: false, hideControlOnEnd: true });

      }

    } else {

      for(var x = 0; x < slider.length; x++) {

        slider[x].reloadSlider({ mode: 'horizontal', slideWidth: 250, minSlides: 2, maxSlides: 3, moveSlides: 1, slideMargin: 0, infiniteLoop: false, hideControlOnEnd: true });

      }

    }

  }

}

$(document).ready(function() {

    //Smooth Scroll
    $('#video-blog ul.navbar.nav a').click(function() {
      if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
        if (target.length) {
          $('html, body').animate({
            scrollTop: target.offset().top
          }, 1000);
          return false;
        }
      }
    });

    bxSliderInit("init");

    $('div.alternate-video-articles').on("click", function() {

      var thumbLoc = $(this).data("carousel");
      var title = $(this).data("title");
      var excerpt = $(this).data("excerpt");
      var img = $(this).data("image");
      var iframe = $(this).data("iframe");

      var parent = $('#'+thumbLoc);

      if(!$(this).find("img").hasClass("active")) {

        parent.find('div.alternate-video-articles img.active').removeClass("active");

        $(this).find("img").addClass("active");

      }

      if(title != parent.find("h3").text()) {

        parent.find("img.featured-image").attr("src", img);

        parent.find("h3").text(title);

        parent.find("div.video-blog-excerpt-container p").text(excerpt);

        parent.find("a[data-toggle='modal']").data("iframe", iframe);

      }     

    });

    $('div.product-modals').on("click", function() {

       var src = $(this).find("iframe")[0].src;
           src = src.substring(0, src.indexOf("autoplay") - 5);
           
           $(this).find("iframe")[0].src = src;

    });

    var waitForFinalEvent = (function () {
        var timers = {};
        return function (callback, ms, uniqueId) {
          if (timers[uniqueId]) {
            clearTimeout (timers[uniqueId]);
          }
          timers[uniqueId] = setTimeout(callback, ms);
        };
    })();

    $(window).resize(function() {

      waitForFinalEvent(function() {

        bxSliderInit("reload");

      }, 300, "bxSlider");

    });

});