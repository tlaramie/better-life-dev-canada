/**
* Shopify Ajaxify Shop.
*
* @uses Modified Shopify jQuery API (link to it)
*
*/

jQuery(document).ready(function() {
//Begin Wrapper

var jQ = jQuery;

/**
* Collection of Selectors for various pieces on the page we need to update
*
* I've tried to keep these as general and flexible as possible, but
* if you are doing your own markup, you may find you need to change some of these.
*
*/
var selectors = {
    // Any elements(s) with this selector will have the total item count put there on add to cart.
    TOTAL_ITEMS: '.cart-item-count',
    TOTAL_PRICE: '.mini-cart-total-price',
    FREE_SHIPPING_MESSAGE: '#free-shipping-message',

    SUBMIT_ADD_TO_CART: 'input.add-to-cart',
    
    FORM_ADD_TO_CART: 'form[action*="/cart/add"]',
    
    FORM_UPDATE_CART: 'form[name=cartform]',
    //The actual Update Button
    FORM_UPDATE_CART_BUTTON: 'form[name=cartform] input[name=update]',
    //All the buttons on the form
    FORM_UPDATE_CART_BUTTONS: 'input[type=image], input.button-update-cart',

    LINE_ITEM_ROW: '.mini-cart-item',
    LINE_ITEM_QUANTITY_PREFIX: '.cart-line-item-quantity-',
    LINE_ITEM_PRICE_PREFIX: '.cart-line-item-price-',

    LINE_ITEM_REMOVE: '.remove a',
    
    EMPTY_CART_MESSAGE: '#empty',
    LINE_ITEM_ADD: '#mini-cart-item-container',
    MINI_CART:'#mini-cart'
};


/**
* Collection of text strings. This is where you would change for a diff language, for example.
*
*/
var text = {
    ITEM: 'Item',
    ITEMS: 'Items'
};

//Convenience method to format money.
//Can just transform the amount here if needed
var formatMoney = function(price) {
    return Shopify.formatMoney(price , '${{ amount }}');
};

//Attach Submit Handler to all forms with the /cart/add action.
jQ(selectors.FORM_ADD_TO_CART).submit(function(e) {
    e.preventDefault();
    //Disable the Add To Cart button, add a disabled class.
    jQ(e.target).find(selectors.SUBMIT_ADD_TO_CART).attr('disabled', true).addClass('disabled');
    jQ(e.target).find(selectors.SUBMIT_ADD_TO_CART).attr('value', 'ITEM ADDED');
    //Can't use updateCartFromForm since you need the item added before you can update (otherwise, would have been more convenient)
    //So, in onItemAdded, we Shopify.getCart() to force the repaint of items in cart.
    Shopify.addItemFromForm(e.target);
});

//We only want to interrupt the UPDATE, not the CHECKOUT process
/*jQ(selectors.FORM_UPDATE_CART_BUTTON).click(function(e) {
    e.preventDefault();
    jQ(e.target.form).find(selectors.FORM_UPDATE_CART_BUTTONS).attr('disabled', true).addClass('disabled');
    Shopify.updateCartFromForm(e.target.form);
});

//Delegate the Remove Link functionality on the cart page.
jQ(selectors.FORM_UPDATE_CART).delegate(selectors.LINE_ITEM_REMOVE, 'click', function(e) {
    e.preventDefault();
    //Get the variant ID from the URL
    var vid = this.href.split('/').pop().split('?').shift();
    Shopify.removeItem(vid);
    jQ(this).parents(selectors.LINE_ITEM_ROW).remove();
});*/

/**
* Shopify.onItemAdded
*
* Triggered by the response when something is added to the cart via the add to cart button.
* This is where you would want to put any flash messaging, for example.
*
* @param object line_item
* @param HTMLelement/String Form HTMLElement, or selector
*/
Shopify.onItemAdded = function(line_item, form) {
    //Default behaviour for this modification:
    //When a Add To Cart form is clicked, we disable the button and apply a class of disabled.
    //Here is where we remove the disabled class, and reactivate the button.
    jQ(form).find(selectors.SUBMIT_ADD_TO_CART).attr('disabled', false).removeClass('disabled');
    //You can add any extra messaging you would want here.
    //Get the state of the cart, which will trigger onCartUpdate
    Shopify.getCart();
    var cartButtonTimer = setTimeout(function(){ resetBagText(form) },1000);
};

function resetBagText(form){
    jQ(form).find(selectors.SUBMIT_ADD_TO_CART).attr('value', 'ADD TO BAG');
};

/**
* This updates the N item/items left in your cart
*
* It's setup to match the HTML used to display the Cart Count on Load. If you change that (in your theme.liquid)
* you will probably want to change the message html here.
* This will update the HTML in ANY element with the class defined in selectors.TOTAL_ITEMS
*
* @param object the cart object.
* @param HTMLElement form. If included, we know its an Update of the CART FORM, which will trigger additional behaviour.
*/
Shopify.onCartUpdate = function(cart, form) {

    // Total Items Update
    var message = '('+cart.item_count+')';
    jQ(selectors.TOTAL_ITEMS).html(message);

    // Price update - any element matching the selector will have their contents updated with the cart price.
    var price = formatMoney(cart.total_price);
    jQ(selectors.TOTAL_PRICE).html(price);
    
    if ( cart.total_price >= 5000 || cart.total_price == 1299 || cart.total_price == 1199 ){
        var shippingMessage = 'Your order qualifies for FREE SHIPPING!';
         jQ(selectors.FREE_SHIPPING_MESSAGE).html(shippingMessage);
    }else{
        var amtForFreeShipping = formatMoney(5000 - cart.total_price);
        var shippingMessage2 = amtForFreeShipping + ' until FREE SHIPPING.';
        jQ(selectors.FREE_SHIPPING_MESSAGE).html(shippingMessage2);
    }

    //If the EMPTY_CART_MESSAGE element exiss, we should show it, and hide the form.
    if( (jQ(selectors.EMPTY_CART_MESSAGE).length > 0) && cart.item_count == 0) {
        jQ(selectors.FORM_UPDATE_CART).hide();
        jQ(selectors.EMPTY_CART_MESSAGE).show();
    }

    // A form was passed in?
   // form = form || false;
    //so it's the cart page form update, trigger behaviours for that page
    //if(form) {
        //Nothing left in cart, we reveal the Nothing in cart content, hide the form.
        if(cart.item_count > 0) {
            //jQ(selectors.LINE_ITEM_ADD).children( 'div:not(:first)' ).remove();
            jQ(selectors.LINE_ITEM_ADD+" div").remove();
            //Loops through cart items, update the prices.
            jQ.each(cart.items, function(index, cartItem) {
                /*if(cartItem.quantity > 1){
                    jQ(selectors.LINE_ITEM_PRICE_PREFIX + cartItem.id).html(formatMoney(cartItem.line_price));
                    jQ(selectors.LINE_ITEM_QUANTITY_PREFIX + cartItem.id).html(cartItem.quantity);
                }else{*/
                    var minicartprice = formatMoney(cartItem.price);
                    var minicartlineprice = formatMoney(cartItem.line_price);
                    var minicartitemprice = formatMoney(cartItem.price);
                    var newitementry = '<div class="row mini-cart-item"><div class="col-md-8"><h5 class="header-'+cartItem.sku+'"><a href="'+cartItem.url+'">'+cartItem.title+'</a></h5><ul><li>QTY: '+cartItem.quantity+'</li><li>Price: '+minicartitemprice+'</li></ul></div><div class="col-md-4"><p>'+minicartlineprice+'</p></div></div>';
                    //jQ(newitementry).insertAfter( '#miniCartTable > tbody > tr#headerRow');
                    //var jqHeaderRow = jQ( '#headerRow');
                    jQ(selectors.LINE_ITEM_ADD).append(newitementry);
                //};
            });

            //And remove any line items with 0
            //jQ(form).find('input[value=0]').parents(selectors.LINE_ITEM_ROW).remove();

            //Since we are on the cart page, reenable the buttons we disabled
            //jQ(form).find(selectors.FORM_UPDATE_CART_BUTTONS).attr('disabled', false).removeClass('disabled');

        }
        //You can add any extra messaging you would want here.
        //successMessage('Cart Updated.');
        dropMiniCartDown( );
    //}
};

function dropMiniCartDown( ){
    var windowWidth = jQ(window).width( );
    var contentWidth = jQ('.container').width( );
    /*var miniCartX = ((windowWidth - contentWidth)/2);
    jQ('#mini-cart').css('right',miniCartX);*/
    jQ('#mini-cart').slideDown(500, 'swing', closeCartTimer());
};

function closeCartTimer(){
    var cartCloseTimer = setTimeout(function(){ closeCart() },3500);
};

function closeCart(){
     jQ('#mini-cart').slideUp(500, 'swing');
};


//What to display when there is an error. You tell me?! I've left in a commented out example.
// You can tie this in to any sort of flash messages, or lightbox, or whatnot you want.
Shopify.onError = function(XMLHttpRequest, textStatus) {
  // Shopify returns a description of the error in XMLHttpRequest.responseText.
  // It is JSON.
  // Example: {"description":"The product 'Amelia - Small' is already sold out.","status":500,"message":"Cart Error"}
  // var data = eval('(' + XMLHttpRequest.responseText + ')');
  // errorMessage(data.message + '(' + data.status + '): ' + data.description);
};

//End Wrapper
});// JavaScript Document